angular.module('starter.controllers', [])

  .controller('AppCtrl', function ($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});
  })

  .controller('HomeCtrl', function ($scope, $rootScope, $ionicModal) {
    var map;
    var uluru = {lat: 21.026567, lng: 105.837015};

    $scope.initMap = function () {
      $scope.directionsService = new google.maps.DirectionsService;
      $scope.directionsDisplay = new google.maps.DirectionsRenderer;


      map = new google.maps.Map(document.getElementById('map'), {
        center: uluru,
        zoom: 16
      });
      $scope.directionsDisplay.setMap(map);


      var marker = new google.maps.Marker({
        position: uluru,
        map: map
      });
      var infoWindow = new google.maps.InfoWindow({map: map});

      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          infoWindow.setPosition(pos);
          infoWindow.setContent('Location found.');
          map.setCenter(pos);
        }, function () {
          //$scope.handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        // Browser doesn't support Geolocation
        $scope.handleLocationError(false, infoWindow, map.getCenter());
      }
    }
    $scope.travelModes = [
      {'name': 'BICYCLING', 'value': 'BICYCLING'},
      {'name': 'DRIVING', 'value': 'DRIVING'},
      {'name': 'TRANSIT', 'value': 'TRANSIT'},
      {'name': 'WALKING', 'value': 'WALKING'}
    ];
    //Setting first option as selected in configuration select
    $scope.travelMode = $scope.travelModes[1].value;
    $scope.calculateAndDisplayRoute = function (directionsService, directionsDisplay, startRoute, endRoute, travelMode) {
      if (startRoute && endRoute && travelMode) {


        $scope.directionsService.route({
          origin: startRoute.name,
          destination: endRoute.name,
          travelMode: travelMode
        }, function (response, status) {
          if (status === 'OK') {
            $scope.directionsDisplay.setDirections(response);
            $scope.closeFindWayModal();
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    }


    $scope.handleLocationError = function (browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    }
    $scope.initMap();

    $scope.myScopeVar = null;
    $scope.getLocation = function (pos) {
      if (pos.geometry) {
        debugger;
        console.log(jQuery('.pac-container').html())
        var pos = {
          lat: pos.geometry.location.lat(),
          lng: pos.geometry.location.lng()
        }
        map.setCenter(pos);
        var marker = new google.maps.Marker({
          position: pos,
          map: map
        });
      }
      setTimeout(function(){
        console.log(jQuery('.pac-container').attr('style'));
        console.log(jQuery('.pac-container').html());
        jQuery('body').append()
      },2000)
    }


    $ionicModal.fromTemplateUrl('my-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });
    $scope.openFindWayModal = function () {
      $scope.modal.show();
    }

    $scope.closeFindWayModal = function () {
      $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
      // Execute action
    });


    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch({
      location: uluru,
      radius: 500,
      type: ['hospital']
    }, callback);

    function callback(results, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          createMarker(results[i]);
        }
      }
    }

    function createMarker(place) {
      var placeLoc = place.geometry.location;
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });
    }
  })
;

